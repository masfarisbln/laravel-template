<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@tampilhome');

Route::get('/register', 'AuthController@tampilregister');

Route::post('/welcome', 'AuthController@tampilnama');

Route::get('/data-tables', 'IndexController@tampildatatables');

// CRUD cast 
// Create
Route::get('/cast/create', 'CastController@create'); // Route menuju form create
Route::post('/cast', 'CastController@store'); // Route untuk menyimpan data ke database

// Read
Route::get('/cast', 'CastController@index'); // Route list Cast
Route::get('/cast/{cast_id}', 'CastController@show'); // Route detail Cast

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // Route menuju ke form edit
Route::put('/cast/{cast_id}', 'CastController@update'); // Route untuk update data berdasarkan id di database

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // Route untuk hapus data di database