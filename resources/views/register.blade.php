@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h2> Buat Account Baru </h2>
    <h4> Sign Up Form </h4>
    <form action="/welcome" method="post">
        @csrf
        First Name : <br>
        <input type="text" name="fname"> <br><br>
        Last Name : <br>
        <input type="text" name="lname"> <br><br>
        Gender <br>
        <input type="radio" name "gender"> Male <br>
        <input type="radio" name "gender"> Female <br>
        <input type="radio" name "gender"> Other <br><br>
        Nationality <br>
        <select name = "bangsa">
          <option value="indo"> Indonesia </option>
          <option value="amerika"> Amerika </option>
          <option value="inggris"> Inggris </option>
        </select> <br><br>
        Language Spoken <br>
        <input type="checkbox" name "bahasa"> Bahasa Indonesia <br>
        <input type="checkbox" name "bahasa"> English <br>
        <input type="checkbox" name "bahasa"> Other <br><br>
        <label> Bio </label> <br>
        <textarea name="bio" rows="10" cols="30"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
    </p>
    @endsection