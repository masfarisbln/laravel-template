<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function tampilhome(){
        return view('home');
    }

    public function tampildatatables(){
        return view('data-table');
    }
}
