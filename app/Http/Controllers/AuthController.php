<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function tampilregister(){
        return view('register');
    }

    public function tampilnama(Request $request) {
        $firstname = $request['fname'];
        $lastname = $request['lname'];
        return view('welcome', compact('firstname','lastname'));
    }
}
